const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];
let colorSpace = "";
colorList.forEach((element) => {
  let colorElement = `<button onclick=doiMau('${element}') class="color-button ${element}" id ="${element}"></button>`;
  colorSpace += colorElement;
});
document.getElementById("colorContainer").innerHTML = colorSpace;

var currentColor = "";

let doiMau = (color) => {
  if (currentColor == "") {
    document.getElementById(color).classList.add("active");
    document.getElementById("house").classList.add(color);
    currentColor = color;
  } else {
    document.getElementById("house").classList.replace(currentColor, color);
    document.getElementById(currentColor).classList.remove("active");
    document.getElementById(color).classList.add("active");
    currentColor = color;
  }
};
